export const DEFAULT_TIMEOUT = 30000;

/**
 * @description：请求配置
 */
export enum HttpStatusCodeEnum {
  OK = 200,
  INTERNAL_SERVER_ERROR = 500,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403
}

/**
 * @description：请求方法
 */
export enum MethodEnum {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE'
}

// 请求响应参数（不包含data）
export interface Result {
  code: string;
  msg: string;
}

// 请求响应参数（包含data）
export interface ResultData<T = any> extends Result {
  data: T;
}

export interface LoginRequestInfo {
  username: string;
  password: string;
}
export interface LoginResponseInfo {
  access_token: string;
}
