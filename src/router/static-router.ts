import { RouteRecordRaw } from 'vue-router';
import { LOGIN_URL } from '@/config';

/**
 * staticRouter (静态路由)
 */
export const staticRouter: RouteRecordRaw[] = [
  {
    path: LOGIN_URL,
    name: 'login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: '登录'
    }
  },
  {
    path: '/',
    component: () => import('@/layout/index.vue'),
    redirect: '/project',
    children: [
      {
        path: '/project',
        component: () => import('@/views/project/index.vue'),
        meta: {
          title: '项目管理'
        }
      },
      {
        path: '/container',
        component: () => import('@/views/module/container.vue'),
        meta: {
          title: '容器管理'
        }
      },
      {
        path: '/microapp',
        component: () => import('@/views/module/microapp.vue'),
        meta: {
          title: '微应用管理'
        }
      },
      {
        path: '/product',
        component: () => import('@/views/product/index.vue'),
        meta: {
          title: '产品管理'
        }
      }
    ]
  }
];

/**
 * errorRouter (错误页面路由)
 */
export const errorRouter = [
  {
    path: '/403',
    name: '403',
    component: () => import('@/components/error-message/403.vue'),
    meta: {
      title: '403页面'
    }
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/components/error-message/404.vue'),
    meta: {
      title: '404页面'
    }
  },
  {
    path: '/500',
    name: '500',
    component: () => import('@/components/error-message/500.vue'),
    meta: {
      title: '500页面'
    }
  },
  // Resolve refresh page, route warnings
  {
    path: '/:pathMatch(.*)*',
    component: () => import('@/components/error-message/404.vue')
  }
];
