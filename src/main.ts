import { createApp } from 'vue';
import App from './App.vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import '@/style/index.scss';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

// vue Router
import router from '@/router';
// pinia store
import pinia from '@/store';
const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(ElementPlus);
app.use(router);
app.use(pinia);
app.mount('#app');
