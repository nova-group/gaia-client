import { ConfigEnv, UserConfig, defineConfig, loadEnv } from 'vite';
import { resolve } from 'path';
import { wrapperEnv } from './build/getEnv';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/

export default defineConfig(({ mode }: ConfigEnv): UserConfig => {
  const root = process.cwd();
  const env = loadEnv(mode, root);
  const viteEnv = wrapperEnv(env);
  return {
    plugins: [vue()],
    resolve: {
      alias: {
        '@': resolve(__dirname, 'src')
      }
    },
    server: {
      host: '0.0.0.0',
      port: viteEnv.VITE_PORT,
      open: viteEnv.VITE_OPEN
    }
  };
});
